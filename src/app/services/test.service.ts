import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { System } from '../models/testmodel';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  baseUrl = 'http://localhost:3000/systems/';
  baseUrl2= 'http://localhost:5000/api/metavault/sources/';

  constructor(private http: HttpClient) { }

  getSystems(): Observable<System[]> {
    return this.http.get<System[]>(this.baseUrl2);
  }

  getSystem(id): Observable<System> {
    return this.http.get<System>(this.baseUrl2 +id)
  }

  createSystem(system: any) {
    return this.http.post<System>(this.baseUrl2, system)
  }

  editSystem(id:string, system: System) {
    return this.http.put<System>(this.baseUrl2 +id, system)
  }

  deleteSystem(id:string): Observable<System>{
    return this.http.delete<System>(this.baseUrl2 +id)
}
}
