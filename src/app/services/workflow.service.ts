import { Injectable } from '@angular/core';
import { FlowElement, LayoutRootElement } from '../models/navigationElement';


@Injectable({
  providedIn: 'root'
})
export class WorkflowService {

  public layoutElements: LayoutRootElement[] = [];

  constructor() {

    // Update Cycle
    const updateCycleLayoutElement = new LayoutRootElement();
    updateCycleLayoutElement.type = 'UpdateCycle';
    updateCycleLayoutElement.link = 'updatecycle';
    updateCycleLayoutElement.title = 'Update Cycle';
    updateCycleLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
         vitae dapibus odio
        tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
        in vehicula vitae, venenatis sed tortor.`;
    updateCycleLayoutElement.modulesElements = [];

    updateCycleLayoutElement.modulesElements.push(
      {
        title: 'Import Information', icon: 'bookmark-icon fa fa-info-circle', description: 'Import information'
        , link: 'importinfo', type: 'UpdateCycle', disabled: true, tag: ['update cycle', 'import information']
      },
      {
        title: 'Update Period', icon: 'bookmark-icon fa fa-refresh', description: 'Manage update period'
        , link: 'updateperiod', type: 'UpdateCycle', disabled: true, tag: ['update cycle', 'update period']
      },
      {
        title: 'Indexation', icon: 'bookmark-icon fa fa-hashtag', description: 'Refresh indexation', link: 'indexation',
        type: 'UpdateCycle', disabled: false, tag: ['update cycle', 'indexation']
      },
      {
        title: 'Frozen Periods', icon: 'bookmark-icon fa fa-snowflake-o', description: 'Manage frozen periods',
        link: 'frozenperiods', type: 'UpdateCycle', disabled: true, tag: ['update cycle', 'frozen periods']
      },
      {
        title: 'Change Requests', icon: 'bookmark-icon fa fa-table', description: 'Manage change requests'
        , link: 'changerequests', type: 'UpdateCycle', disabled: true, tag: ['update cycle', 'change requests']
      },
      {
        title: 'Transfer Requests', icon: 'bookmark-icon fa fa-arrows-h', description: 'Manage transfer requests',
        link: 'transferrequests', type: 'UpdateCycle', disabled: true, tag: ['update cycle', 'transfer requests']
      });

    this.layoutElements.push(updateCycleLayoutElement);

    // Users & Groups
    const userGroupsLayoutElement = new LayoutRootElement();
    userGroupsLayoutElement.type = 'UsersAndGroups';
    userGroupsLayoutElement.link = 'usersandgroups';
    userGroupsLayoutElement.title = 'Users & Groups';
    userGroupsLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
             vitae dapibus odio
            tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
            in vehicula vitae, venenatis sed tortor.`;
    userGroupsLayoutElement.modulesElements = [];

    userGroupsLayoutElement.modulesElements.push(
      {
        title: 'Users', icon: 'bookmark-icon fa fa-user', description: 'Manage users', link: 'users', type: 'UsersAndGroups',
        disabled: false, tag: ['users and groups']
      },
      {
        title: 'Groups', icon: 'bookmark-icon fa fa-users', description: 'Manage groups'
        , link: 'groups', type: 'UsersAndGroups', disabled: false, tag: ['users and groups']
      });

    this.layoutElements.push(userGroupsLayoutElement);

    // Communication
    const comunicationLayoutElement = new LayoutRootElement();
    comunicationLayoutElement.type = 'Communication';
    comunicationLayoutElement.link = 'communication';
    comunicationLayoutElement.title = 'Communication';
    comunicationLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                 vitae dapibus odio
                tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                in vehicula vitae, venenatis sed tortor.`;
    comunicationLayoutElement.modulesElements = [];

    comunicationLayoutElement.modulesElements.push(
      {
        title: 'Emails', icon: 'bookmark-icon fa fa-envelope-o', description: 'Manage emails', link: 'mailtemplates',
        type: 'Communication', disabled: false, tag: ['communication', 'emails']
      },
      {
        title: 'Email Group', icon: 'bookmark-icon fa fa-share', description: 'Email management', link: 'groupmails',
        type: 'Communication', disabled: false, tag: ['communication', 'email group']
      },
      {
        title: 'Email Diffusion', icon: 'bookmark-icon fa fa-share', description: 'Email diffusion', link: 'generatedmails',
        type: 'Communication', disabled: false, tag: ['communication', 'email diffusion']
      },
      {
        title: 'Messages', icon: 'bookmark-icon fa fa-commenting-o', description: 'Manage messages', link: 'messages',
        type: 'Communication', disabled: false, tag: ['communication', 'messages']
      });

    this.layoutElements.push(comunicationLayoutElement);

    // Reporting
    const reportingLayoutElement = new LayoutRootElement();
    reportingLayoutElement.type = 'Reporting';
    reportingLayoutElement.link = 'reporting';
    reportingLayoutElement.title = 'Reporting';
    reportingLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                     vitae dapibus odio
                    tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                    in vehicula vitae, venenatis sed tortor.`;
    reportingLayoutElement.modulesElements = [];

    reportingLayoutElement.modulesElements.push(
      {
        title: 'Reports', icon: 'bookmark-icon fa fa-line-chart', description: 'Generate reports', link: 'scorecards',
        type: 'Reporting', disabled: true, tag: ['reporting', 'reports']
      },
      {
        title: 'Snapshot Dates', icon: 'bookmark-icon fa fa-calendar', description: 'Manage snapshot dates'
        , link: 'scorecards', type: 'Reporting', disabled: true, tag: ['reporting', 'snapshot dates']
      });

    this.layoutElements.push(reportingLayoutElement);

    // Exchange Rate
    const exchangeRateLayoutElement = new LayoutRootElement();
    exchangeRateLayoutElement.type = 'ExchangeRate';
    exchangeRateLayoutElement.link = 'exchangerate';
    exchangeRateLayoutElement.title = 'Exchange Rate';
    exchangeRateLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                     vitae dapibus odio
                    tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                    in vehicula vitae, venenatis sed tortor.`;
    exchangeRateLayoutElement.modulesElements = [];

    exchangeRateLayoutElement.modulesElements.push(
      {
        title: 'Currencies', icon: 'bookmark-icon fa fa-dollar', description: 'Manage currencies', link: 'scorecards',
        type: 'ExchangeRate', disabled: true, tag: ['exchange rate', 'currencies']
      },
      {
        title: 'Exchange Rates', icon: 'bookmark-icon fa fa-exchange', description: 'Manage exchange rates'
        , link: 'scorecards', type: 'ExchangeRate', disabled: true, tag: ['exchanges rates']
      });

    this.layoutElements.push(exchangeRateLayoutElement);

    // Monitoring
    const monitoringLayoutElement = new LayoutRootElement();
    monitoringLayoutElement.type = 'Monitoring';
    monitoringLayoutElement.link = 'monitoring';
    monitoringLayoutElement.title = 'Monitoring';
    monitoringLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                     vitae dapibus odio
                    tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                    in vehicula vitae, venenatis sed tortor.`;
    monitoringLayoutElement.modulesElements = [];

    monitoringLayoutElement.modulesElements.push(
      {
        title: 'Jobs', icon: 'bookmark-icon fa fa-list', description: 'View jobs', link: 'jobs', type: 'Monitoring',
        disabled: false, tag: ['monitoring', 'jobs']
      },
      {
        title: 'History', icon: 'bookmark-icon fa fa-history', description: 'View history'
        , link: 'history', type: 'Monitoring', disabled: false, tag: ['monitoring', 'history']
      });

    this.layoutElements.push(monitoringLayoutElement);

    // Structure Management
    const structureLayoutElement = new LayoutRootElement();
    structureLayoutElement.type = 'Structure';
    structureLayoutElement.link = 'structure';
    structureLayoutElement.title = 'Structure Management';
    structureLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                         vitae dapibus odio
                        tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                        in vehicula vitae, venenatis sed tortor.`;
    structureLayoutElement.modulesElements = [];

    structureLayoutElement.modulesElements.push(
      {
        title: 'Top Management', icon: 'bookmark-icon fa fa-home', description: 'Manage top'
        , link: 'scorecards', type: 'Structure', disabled: true, tag: ['structure', 'top management']
      },
      {
        title: 'Level Management', icon: 'bookmark-icon fa fa-sitemap', description: 'Manage levels', link: 'structure',
        disabled: true, type: 'Structure', tag: ['structure', 'level management']
      },
      {
        title: 'Duplications', icon: 'bookmark-icon fa fa-clone', description: 'Manage duplications', link: 'scorecards',
        type: 'Structure', disabled: true, tag: ['structure', 'duplications']
      },
      {
        title: 'Gray Zone', icon: 'bookmark-icon fa fa-bar-chart', description: 'Manage gray zone', link: 'scorecards',
        type: 'Structure', disabled: true, tag: ['structure', 'gray zone']
      });

    this.layoutElements.push(structureLayoutElement);

    // Maturity Management
    const maturityLayoutElement = new LayoutRootElement();
    maturityLayoutElement.type = 'Maturities';
    maturityLayoutElement.link = 'maturities';
    maturityLayoutElement.title = 'Maturity';
    maturityLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                             vitae dapibus odio
                            tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                            in vehicula vitae, venenatis sed tortor.`;
    maturityLayoutElement.modulesElements = [];

    maturityLayoutElement.modulesElements.push(
      {
        title: 'Maturities', icon: 'bookmark-icon fa fa-star', description: 'Manage maturities', link: 'maturities',
        type: 'Maturities', disabled: false, tag: ['maturities']
      },
      {
        title: 'Workflows', icon: 'bookmark-icon fa fa-star-o', description: 'Manage maturity workflows',
        link: 'maturitystrategies', type: 'Maturities', disabled: false, tag: ['maturities', 'workflows']
      },
      {
        title: 'Checklists', icon: 'bookmark-icon fa fa-list', description: 'Manage checklists', link: 'checklists',
        type: 'Maturities', disabled: false, tag: ['maturities', 'checklists']
      },
      {
        title: 'Checklists items', icon: 'bookmark-icon fa fa-list-ul', description: 'Manage checklists items',
        link: 'deliverables', type: 'Maturities', disabled: false, tag: ['maturities', 'checklists items']
      });

    this.layoutElements.push(maturityLayoutElement);

    // Node Configuration
    const nodeLayoutElement = new LayoutRootElement();
    nodeLayoutElement.type = 'Nodes';
    nodeLayoutElement.link = 'nodes';
    nodeLayoutElement.title = 'Node Configuration';
    nodeLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                             vitae dapibus odio
                            tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                            in vehicula vitae, venenatis sed tortor.`;
    nodeLayoutElement.modulesElements = [];

    nodeLayoutElement.modulesElements.push(
      {
        title: 'Properties', icon: 'bookmark-icon fa fa-cog', description: 'Manage properties'
        , link: 'properties', type: 'Nodes', disabled: false, tag: ['node configuration', 'properties']
      },
      {
        title: 'Properties set', icon: 'bookmark-icon fa fa-cogs', description: 'Manage properties set'
        , link: 'propertysets', type: 'Nodes', disabled: false, tag: ['node configuration', 'properties set']
      },
      {
        title: 'Flags', icon: 'bookmark-icon fa fa-flag', description: 'Manage flags'
        , link: 'flags', type: 'Nodes', disabled: false, tag: ['node configuration', 'flags']
      },
      {
        title: 'Sections', icon: 'bookmark-icon fa fa-puzzle-piece', description: 'Manage sections'
        , link: 'sections', type: 'Nodes', disabled: false, tag: ['node configuration', 'sections']
      },
      {
        title: 'Operational Kpis', icon: 'bookmark-icon fa fa-trophy', description: 'Manage operation kpis', link: 'kpis',
        type: 'Nodes', disabled: false, tag: ['node configuration', 'operational kpis']
      },
      {
        title: 'Imports', icon: 'bookmark-icon fa fa-download', description: 'Manage imports', link: 'imports', type: 'Nodes',
        disabled: true, tag: ['node configuration', 'imports']
      },
      {
        title: 'Exports', icon: 'bookmark-icon fa fa-upload', description: 'Manage exports', link: 'exports', type: 'Nodes',
        disabled: true, tag: ['node configuration', 'exports']
      },
      {
        title: 'Recycle Bin', icon: 'bookmark-icon fa fa-recycle', description: 'Manage recycle bin', link: 'deletenodes',
        type: 'Nodes', disabled: false, tag: ['node configuration', 'recycle bin']
      });

    this.layoutElements.push(nodeLayoutElement);

    // Scorecards
    const scorecardLayoutElement = new LayoutRootElement();
    scorecardLayoutElement.type = 'Scorecards';
    scorecardLayoutElement.link = 'scorecards';
    scorecardLayoutElement.title = 'Scorecard';
    scorecardLayoutElement.description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla viverra efficitur mauris,
                             vitae dapibus odio
                            tristique a. Vivamus sem sapien, fermentum lobortis euismod mattis, feugiat eu magna. Sed mi sapien, vehicula
                            in vehicula vitae, venenatis sed tortor.`;
    scorecardLayoutElement.modulesElements = [];

    scorecardLayoutElement.modulesElements.push(
      {
        title: 'Scorecards', icon: 'bookmark-icon fa fa-table', description: 'Configure scorecards', link: 'scorecards',
        type: 'Scorecards', disabled: false, tag: ['scorecards']
      },
      {
        title: 'Scorecards Views', icon: 'bookmark-icon fa fa-table', description: 'Configure scorecard views',
        link: 'scorecardviews', type: 'Scorecards', disabled: false, tag: ['scorecards views']
      },
      {
        title: 'Effects', icon: 'bookmark-icon fa fa-list-ol', description: 'Configure effects', link: 'effects',
        type: 'Scorecards', disabled: false, tag: ['scorecards', 'effects']
      },
      {
        title: 'Financial States', icon: 'bookmark-icon fa fa-bars', description: 'Configure financial states',
        link: 'financialstates', type: 'Scorecards', disabled: false, tag: ['scorecards', 'financial states']
      }
    );

    this.layoutElements.push(scorecardLayoutElement);
  }

}
