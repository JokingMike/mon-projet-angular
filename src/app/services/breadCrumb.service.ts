import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class BreadcrumbService {
    
    private toggleSwitched = new BehaviorSubject<boolean>(false);
    breadcrumbChanged = new Subject<string>();
    cast = this.toggleSwitched.asObservable();

    constructor() {}

    swithToggle(toggle: boolean) {
        this.toggleSwitched.next(toggle);
    }

    changeBreadcrumb(bread: string) {
        this.breadcrumbChanged.next(bread);
    }


}