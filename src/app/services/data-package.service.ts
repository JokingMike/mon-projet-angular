import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { PackageModel } from '../models/dataPModel';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
  })
export class DataPackageService {
    
    baseUrl = 'http://localhost:3000/dataPackage/';
    baseUrl2 = 'http://localhost:5000/api/metavault/datapackages/'

    constructor(private http: HttpClient) { }

    deletePackage(id:number): Observable<PackageModel>{
        return this.http.delete<PackageModel>(this.baseUrl2 +id)
    }

    createPackage(dpackage:PackageModel) {
      return this.http.post<PackageModel>(this.baseUrl2, dpackage)
    }

    editPackage(id:string, dpackage: PackageModel) {
        return this.http.put<PackageModel>(this.baseUrl2 +id, dpackage)
      }
}