import { Table } from '../models/table';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TableModel } from '../models/tablelist';
import { StagingAreaTableRequest } from '../models/stagingAreaTableRequest';

@Injectable({
    providedIn: 'root'
  })

export class TableService {
    baseUrl = 'http://localhost:3000/stagingTable?dataPackageId=';
    baseUrl2 = 'http://localhost:5000/api/metavault/staging/';
    constructor(private http: HttpClient) { }

    getStaging(id): Observable<Table> {
      return this.http.get<Table>(this.baseUrl2 + id +'/table' );
    }

    getAvailableStagingTables() {
      return this.http.get<TableModel[]>(this.baseUrl2 +'tables')
    }

    createStagingAreaTable(table) {
      return this.http.post<Table>(this.baseUrl2 +'tables', table)
    }

    createStagingAreaColumn(column) {
      return this.http.post<StagingAreaTableRequest>(this.baseUrl2 +'stagingColumns', column)
    }

    deleteStagingTable(idTable) {
      return this.http.delete<Table>(this.baseUrl2+ 'tables/' + idTable )
    }
}

/*
"Id" = "a37df7a3-8ef3-481f-8d4b-49e4dd100cd0"
"DataPackageId" = "b2a11cf4-967f-4adf-bf55-9b8b2d15e275"
"TargetSchemaName" = "stg"
"TargetTableName" = "sales_orders"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:Id" = "5ec90e1d-9ada-4521-bf9c-4631b9cfba6f"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:PhysicalName" = "SalesOrdDesc"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:Type:Name" = "string"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:Type:Length" = "500"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:Type:Precision" = "0"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:TechnicalDescription" = "Sales Order Description"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:BusinessDescription" = "Sales Order Description"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:BusinessName" = "Description"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:Required" = "False"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:IsSequence" = "False"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:IsHashDifference" = "False"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:IsRecordSource" = "False"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:IsLoadDateTime" = "False"
"Columns:5ec90e1d-9ada-4521-bf9c-4631b9cfba6f:HardRuleId" = ""
*/