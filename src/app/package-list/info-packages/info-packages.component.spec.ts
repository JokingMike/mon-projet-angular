import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoPackagesComponent } from './info-packages.component';

describe('InfoPackagesComponent', () => {
  let component: InfoPackagesComponent;
  let fixture: ComponentFixture<InfoPackagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoPackagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoPackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
