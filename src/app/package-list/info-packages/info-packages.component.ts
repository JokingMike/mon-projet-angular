import { Component, Input } from '@angular/core';
import { PackageModel } from 'src/app/models/dataPModel';

@Component({
  selector: 'tr[app-info-packages]',
  templateUrl: './info-packages.component.html',
  styleUrls: ['./info-packages.component.scss']
})
export class InfoPackagesComponent {

  @Input() package: PackageModel;

  constructor() { }
  
}
