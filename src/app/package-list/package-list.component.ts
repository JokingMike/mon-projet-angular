import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'
import { DataPackageService } from '../services/data-package.service';
import { System } from '../models/testmodel';
import { TestService } from '../services/test.service';
import { TableService } from '../services/table.service'
import { PackageModel } from '../models/dataPModel';
import { BreadcrumbService } from '../services/breadCrumb.service';
import { fadeInOut } from '../animations/fade';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TableModel } from '../models/tablelist';
import { Table } from '../models/table';

@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.component.html',
  styleUrls: ['./package-list.component.scss'],
  providers: [DataPackageService, TestService, TableService],
  animations: [fadeInOut]
})
export class PackageListComponent implements OnInit {

  system: System;
  systemTest:System;
  tableList: TableModel[];
  packageToUpdate: PackageModel = null;
  pack: PackageModel[];
  allowEdit: boolean;
  datapackForStaging: PackageModel;
  public sourceId;
  loading = false;
  createPackageForm: FormGroup;
  stagingTable: Table;
  quality: string[] = [ 'None', 'Bad', 'Poor', 'Fair', 'Good', 'Excellent' ]


  constructor(private route: ActivatedRoute, private router: Router, private dataPackageService: DataPackageService, 
    private testService: TestService, private tableService: TableService,  private breadcrumbService: BreadcrumbService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {this.sourceId = params.get('id');
    });
    this.loadSystem();
    this.onChangeBreadcrumb();
    this.breadcrumbService.cast.subscribe(toggle=> 
      { 
        this.allowEdit = toggle;    
      }
    );
    this.createPackageForm = new FormGroup({
      name: new FormControl('',Validators.required),
      isFullLoad: new FormControl('',Validators.required),
      deliverySchedule: new FormControl('',Validators.required),
      technicalDescription: new FormControl('',Validators.required),
      businessDescription: new FormControl('',Validators.required),
      expectedQuality: new FormControl('',Validators.required),
      refreshType: new FormControl('',Validators.required),
      formatInfo: new FormControl('',Validators.required)
    })
  
  };

  onCreatePackage() {
    let packageToCreate= Object.assign(this.createPackageForm.value, {Source: {name: this.sourceId}})
    this.dataPackageService.createPackage(packageToCreate).subscribe(()=> {
    this.loadSystem();
    }); 
  }

  onUpdatePackage() {
    console.log(this.packageToUpdate);
    this.dataPackageService.editPackage(this.packageToUpdate.id, this.packageToUpdate).subscribe(() => { 
      this.loadSystem(); 
    })
  }

  onSelect(id) {
    this.router.navigate(['source/'+this.sourceId +'/table/'+id+'/'+id])
    this.onChangeBreadcrumb();
  }

  onSelectTable(name) {
    let stagingTableToCreate =  { TargetSchemaName: name, TargetTableName: name, DataPackage: {Id: this.datapackForStaging.id}} 
    this.tableService.createStagingAreaTable(stagingTableToCreate).subscribe(response => {
      this.stagingTable = response;
      console.log("id:"+this.stagingTable.id)
      this.router.navigate(['source/'+this.sourceId +'/table/'+this.datapackForStaging.id+'/'+this.stagingTable.id])  
    })    
  }

  updateEl(el) {
    this.packageToUpdate = JSON.parse(JSON.stringify(el));
  }

  onDeletePackage(id:number, index:number) {
    this.dataPackageService.deletePackage(id).subscribe(
      () => this.loadSystem()
    );
  }
  
  loadSystem() {
    this.loading = true;
    this.testService.getSystem(this.sourceId).subscribe((system: System) => {
      this.system = system;
      this.pack = system.dataPackages;
      this.loading = false;
      this.loadTableList();   
    }, error => {
      console.log(error)
    });
  }

  loadTableList() {
    this.tableService.getAvailableStagingTables().subscribe((tableList:TableModel[]) => {
      this.tableList = tableList;
    }, error => {
      console.log(error)
    })
  }

  onChangeBreadcrumb(route:string="Package") {
    this.breadcrumbService.changeBreadcrumb(route);
  }

  resetForm2() {
    this.createPackageForm.reset();
  }

}
