import { Component, OnInit } from '@angular/core';
import { BreadcrumbService} from '../services/breadCrumb.service';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {

  page: string = '';
  toggleValue: boolean;

  private subscription: Subscription;
 
  public sourceId;

  constructor(private breadcrumbService: BreadcrumbService, private location: Location) { }

  ngOnInit() { 
    this.breadcrumbService.cast.subscribe(toggle=> 
      { 
        this.toggleValue = toggle;   
      }
    );

    this.subscription = this.breadcrumbService.breadcrumbChanged
    .subscribe(
      (bread: string) => { 
        this.page = bread;
        console.log(this.page);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  navigateBack() {
    this.location.back(); 
  }

  switchToggle() {
    this.breadcrumbService.swithToggle(!this.toggleValue);
  }
}
