import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnService } from '../services/column.service';
import { TableService } from '../services/table.service';
import { Table } from '../models/table';
import { TestService } from '../services/test.service';
import { BreadcrumbService } from '../services/breadCrumb.service';
import { ColumnTest } from '../models/columnTest';
import { fadeInOut } from '../animations/fade';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [ColumnService, TableService, TestService],
  animations: [fadeInOut]
})

export class TableComponent implements OnInit {

  stagingTable: Table;
  columns: ColumnTest[];
  public packageId;
  public stagingId;
  stagingTableName: string;
  current: number = null;
  show: boolean = false;
  clicked: boolean = false;
  allowEdit: boolean;
  loading = false;
  createColumnForm: FormGroup;
  
  constructor(private router: Router, private route: ActivatedRoute, 
    private breadcrumbService: BreadcrumbService, 
    private tableService: TableService, private location: Location) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {this.packageId = params.get('idPackage');});
    this.route.paramMap.subscribe(params => {this.stagingId = params.get('idStaging');});
    
    this.createColumnForm = new FormGroup({
      PhysicalName: new FormControl('',Validators.required),
      DataType: new FormControl('',Validators.required),
      TechnicalDescription: new FormControl('',Validators.required),
      BusinessDescription: new FormControl('',Validators.required),
      BusinessName: new FormControl('',Validators.required),
      Required: new FormControl('',Validators.required),
      Computed: new FormControl('',Validators.required),
    }),
    this.breadcrumbService.cast.subscribe(toggle=> 
      { 
        this.allowEdit = toggle;    
      }
    );
    this.loading = true;
    
    this.loadTable();
    this.onChangeBreadcrumb();
    this.breadcrumbService.cast.subscribe(toggle=> 
      { 
        this.allowEdit = toggle;    
      }
    );
  }

  onCreateColumn() {
    let columnToCreate= Object.assign(this.createColumnForm.value, {StagingTable: {name: this.stagingTableName, id: this.stagingId}})
    this.tableService.createStagingAreaColumn(columnToCreate).subscribe(()=> {
      this.loadTable();
    }); 
  }

  unAssignTable(){
    this.tableService.deleteStagingTable(this.stagingId).subscribe(
      () =>{
        this.location.back();
      }
    )
  }

  navigateBack() {
    this.router.navigate(['source/'+this.packageId ])
  }

  toggle(i) {
    this.show = !this.show; 
    this.current = i;
    this.clicked = true;
    console.log(i);
    console.log(this.show)
  }

  toggle2(i) {
    if (this.clicked == true) {
      if(this.show == true) {
        if(this.current != i) {
          return true;
        }    
      }
      if(this.show == false) {
        this.current == null;
      }
    } 
  }

  onChangeBreadcrumb(route:string="Staging") {
    this.breadcrumbService.changeBreadcrumb(route);
  }

  resetForm2() {
    this.createColumnForm.reset();
  }

  loadTable() {
    this.tableService.getStaging(this.packageId).subscribe((table: Table) => {
      this.stagingTable = table;
      this.stagingId = table.id;
      this.stagingTableName = table.targetTableName;
      this.columns = this.stagingTable.stagingAreaColumns;
      console.log(this.stagingId);
      this.loading = false;
  },error => {
    console.log(error)
  });
  }
}
