import { ColumnTest } from './columnTest'

export class Table {
    id: string;
    dataPackageId: string;
    targetSchemaName: string;
    targetTableName: string;
    stagingAreaColumns: ColumnTest[];
}