import { Parent } from './parentDataPck';

export interface StagingAreaTableRequest {
    targetSchemaName: string;
    targetTableName: string
    dataPackage: Parent;
}