import { System } from './testmodel';

export interface PackageModel {
    id: string;
    self: string;
    name: string;
    isFullLoad: boolean;
    deliverySchedule: string;
    technicalDescription: string;
    businessDescription: string;
    expectedQuality: string;
    refreshType: string;
    formatInfo: string;
    source: System;
    hasADataPackage: boolean;

  }


