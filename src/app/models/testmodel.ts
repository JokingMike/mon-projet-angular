import { PackageModel } from './dataPModel';

export interface System {
    id: string;
    self: string;
    name: string;
    code: string;
    technicalDescription: string;
    businessDescription: string;
    version: string;
    qualityType: string;
    systemAdministrator: string;
    dataSteward: string;
    numberOfDataPackages: number;
    dataPackages: PackageModel[];
}