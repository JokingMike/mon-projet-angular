import { Parent } from './parentDataPck';

export interface StagingAreaTableColumnRequest {
    id: string;
    physicalName: string;
    dataType: string;
    technicalDescription: string;
    businessDescription: string;
    businessName: string;
    required: boolean;
    computed: boolean;
    stagingTable: Parent;
    HardRuleId: string; 
}


/*
public class StagingAreaColumnRequest
    {
        public string Id { get; set; }

        public string PhysicalName { get; set; }

        public string DataType { get; set; }

        public string TechnicalDescription { get; set; }

        public string BusinessDescription { get; set; }

        public string BusinessName { get; set; }

        public bool Required { get; set; }

        public bool Computed { get; private set; }

        public Parent StagingTable { get; set; }

        public Guid? HardRuleId { get; set; }
    }
    */