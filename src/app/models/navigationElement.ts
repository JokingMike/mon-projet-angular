export class BreadcrumElement {
    name: string;
    link: string;
    isActive: boolean;
    subElements: BreadcrumElement[];
}

export class LayoutRootElement {
    title: string;
    description: string;
    modulesElements: FlowElement[];
    type: string;
    link: string;
}

export class FlowElement {
    title: string;
    icon: string;
    description: string;
    link: string;
    disabled: boolean;
    type: string;
    tag: string[];
}
