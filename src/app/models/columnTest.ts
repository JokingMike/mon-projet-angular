export class ColumnTest {
    id: string;
    physicalName: string;
    dataType: string;
    technicalDescription: string;
    businessDescription: string;
    required: boolean;
    hardRuleId: string;
    computed: boolean;
    stagingTableId: string;
}