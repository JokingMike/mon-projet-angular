import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BreadcrumbService } from '../services/breadCrumb.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

    allowEdit: boolean;

    constructor(private route: Router,  private breadcrumbService: BreadcrumbService) {}

    ngOnInit() {
        this.breadcrumbService.cast.subscribe(toggle=> 
            { 
              this.allowEdit = toggle;    
              console.log(this.allowEdit);
            }
        );
    }

    public logout(): void {
        this.route.navigate(['/login']);
    }
}
