import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescSourcesComponent } from './desc-sources.component';

describe('DescSourcesComponent', () => {
  let component: DescSourcesComponent;
  let fixture: ComponentFixture<DescSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
