import { Component, Input } from '@angular/core';
import { System } from 'src/app/models/testmodel';

@Component({
  selector: 'tr[app-desc-sources]',
  templateUrl: './desc-sources.component.html',
  styleUrls: ['./desc-sources.component.scss']
})

export class DescSourcesComponent {

  @Input() source: System;
  current: number = null;

  constructor() {}

}
