import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { TestService } from '../services/test.service';
import { System } from '../models/testmodel';
import { BreadcrumbService } from '../services/breadCrumb.service';
import { fadeInOut } from '../animations/fade';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [TestService],
  animations: [fadeInOut]
})

export class HomeComponent implements OnInit {

  systems: System[];
  systemToUpdate: System;
  allowEdit: boolean;
  loading = false;
  createSystemForm: FormGroup;
  quality: string[] = [ 'None', 'Bad', 'Poor', 'Fair', 'Good', 'Excellent' ]
  
  constructor(private router: Router, private testService: TestService, 
    private breadcrumbService: BreadcrumbService) { }

  ngOnInit() {
    this.loadSystems();
    this.onChangeBreadcrumb();
    this.breadcrumbService.cast.subscribe(toggle=> 
      { 
        this.allowEdit = toggle;    
      }
    );
    this.createSystemForm = new FormGroup({
      code: new FormControl('',Validators.required),
      name: new FormControl('',Validators.required),
      technicalDescription: new FormControl('',Validators.required),
      businessDescription: new FormControl('',Validators.required),
      version: new FormControl('',Validators.required),
      dataSteward: new FormControl('',Validators.required),
      systemAdministrator: new FormControl('',Validators.required),
      qualityType: new FormControl('',Validators.required), 
    })
  }

  onUpdateSystem() {
    this.testService.editSystem(this.systemToUpdate.id, this.systemToUpdate).subscribe(()=> {
      this.loadSystems();
    });
  }

  onCreateSystem() {
    this.testService.createSystem(this.createSystemForm.value).subscribe(()=> {
      this.loadSystems();
      this.resetForm2();
    });
  }

  onSelect(el) {
    this.router.navigate(['/source', el.name])
  }

  updateEl(el) {
    this.systemToUpdate = JSON.parse(JSON.stringify(el));
    
  }

  onDeleteSystem(id:string) {
    this.testService.deleteSystem(id).subscribe(() => {
      this.loadSystems();
    });
  }

  loadSystems() {
    this.loading = true;
    this.testService.getSystems().subscribe((systems: System[]) => {
      this.systems = systems;
      this.loading = false;
    }, error => {
      console.log(error)
    });
  }

  onChangeBreadcrumb(route:string="Source") {
    this.breadcrumbService.changeBreadcrumb(route);
  }

  resetForm2() {
    this.createSystemForm.reset();
  }
}
