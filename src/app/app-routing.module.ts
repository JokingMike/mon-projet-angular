import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PackageListComponent } from './package-list/package-list.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: '', redirectTo: '/source', pathMatch: 'full'},
  { path: 'source', component: HomeComponent },
  { path: 'source/:id', component: PackageListComponent},
  { path: 'source/:id/table/:idPackage/:idStaging', component: TableComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
